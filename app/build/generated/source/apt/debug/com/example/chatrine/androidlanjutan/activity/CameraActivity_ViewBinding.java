// Generated code from Butter Knife. Do not modify!
package com.example.chatrine.androidlanjutan.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.chatrine.androidlanjutan.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CameraActivity_ViewBinding implements Unbinder {
  private CameraActivity target;

  private View view2131296326;

  @UiThread
  public CameraActivity_ViewBinding(CameraActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CameraActivity_ViewBinding(final CameraActivity target, View source) {
    this.target = target;

    View view;
    target.ivHasilFoto = Utils.findRequiredViewAsType(source, R.id.ivHasilFoto, "field 'ivHasilFoto'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.fabCapture, "field 'fabCapture' and method 'onViewClicked'");
    target.fabCapture = Utils.castView(view, R.id.fabCapture, "field 'fabCapture'", FloatingActionButton.class);
    view2131296326 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CameraActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivHasilFoto = null;
    target.fabCapture = null;

    view2131296326.setOnClickListener(null);
    view2131296326 = null;
  }
}
